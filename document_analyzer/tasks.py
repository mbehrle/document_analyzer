from __future__ import unicode_literals

import logging

from django.apps import apps
from django.db import OperationalError

# from documents.models import DocumentVersion
from lock_manager import LockError
from mayan.celery import app

from .literals import DO_ANALYZER_RETRY_DELAY, LOCK_EXPIRE
# from .models import Result

logger = logging.getLogger(__name__)


@app.task(bind=True, default_retry_delay=DO_ANALYZER_RETRY_DELAY, ignore_result=True)
def task_do_analyze(self, document_version_pk):
    Lock = apps.get_model(
        app_label='lock_manager', model_name='Lock'
    )

    lock_id = 'task_do_analyze_doc_version-%d' % document_version_pk
    try:
        logger.debug('trying to acquire lock: %s', lock_id)
        # Acquire lock to avoid doing Analyze on the same document version more than
        # once concurrently
        # lock = Lock.acquire_lock(lock_id, LOCK_EXPIRE)
        lock = Lock.objects.acquire_lock(lock_id, LOCK_EXPIRE)
        logger.debug('acquired lock: %s', lock_id)
        document_version = None
        try:
            DocumentVersion = apps.get_model(
                app_label='documents', model_name='DocumentVersion'
            )
            document_version = DocumentVersion.objects.get(pk=document_version_pk)
            logger.info(
                'Starting document Analyzer for document version: %s',
                document_version
            )

            Result = apps.get_model(
                app_label='document_analyzer', model_name='Result'
            )
            Result.objects.process_version(document_version)
            # TextExtractor.process_document_version(document_version)
        except OperationalError as exception:
            logger.warning(
                'Analyzer error for document version: %d; %s. Retrying.',
                document_version_pk, exception
            )
            raise self.retry(exc=exception)
        except Exception as exception:
            logger.error(
                'Analyzer error for document version: %d; %s', document_version_pk,
                exception
            )
        else:
            logger.info(
                'Analyzer complete for document version: %s', document_version
            )
        finally:
            lock.release()
    except LockError:
        logger.debug('unable to obtain lock: %s' % lock_id)


@app.task(bind=True, default_retry_delay=DO_ANALYZER_RETRY_DELAY, ignore_result=True)
def task_do_rebuild_all_analyzers(self):
    Lock = apps.get_model(
        app_label='lock_manager', model_name='Lock'
    )

    if Lock.check_existing(name__startswith='document_indexing_task_update_index_document'):
        # A document index update is happening, wait
        raise self.retry()

    try:
        # lock = Lock.acquire_lock(
        #    'document_indexing_task_do_rebuild_all_indexes'
        # )
        lock = Lock.objects.acquire_lock('document_indexing_task_do_rebuild_all_indexes', LOCK_EXPIRE)
    except LockError as exception:
        # Another rebuild is happening, retry later
        raise self.retry(exc=exception)
    else:
        try:
            IndexInstanceNode.objects.rebuild_all_indexes()
        finally:
            lock.release()
